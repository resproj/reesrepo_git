﻿using Core.Common.Contracts;
using Core.Common.Exceptions;
using EESProject.Client.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Client.Contracts
{
    [ServiceContract]
    public interface IEnergyConsumptionService : IServiceContract
    {
        [OperationContract]
        [FaultContract(typeof(NotFoundException))]
        Podatak[] DataRetrieval(string geoAreaCode, DateTime begin, DateTime end);

        [OperationContract]
        string[] GetGeoAreaCodes();

        [OperationContract]
        string OnDemandDatabaseUpdate();
    }
}
