﻿using Core.Common.Exceptions;
using EESProject.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business.Contracts
{
    [ServiceContract]
    public interface IEnergyConsumptionService
    {
        [OperationContract]
        [FaultContract(typeof(NotFoundException))]
        Podatak[] DataRetrieval(string geoAreaCode, DateTime begin, DateTime end);

        [OperationContract]
        string[] GetGeoAreaCodes();

        [OperationContract]
        string OnDemandDatabaseUpdate();
    }
}
