﻿using Core.Common.Contracts;
using Core.Common.Core;
using System;
using System.Runtime.Serialization;

namespace EESProject.Business.Entities
{
    [DataContract]
    public class Podatak : EntityBase, IIdentifiableEntity
    {
        [DataMember]
        public int DataId { get; set; }

        [DataMember]
        public int LokId { get; set; }

        [DataMember]
        public string LocationCode { get; set; }

        [DataMember]
        public decimal Vrednost { get; set; }

        [DataMember]
        public DateTime Vreme { get; set; }

        // Navigation properties
        public GeoArea GeoPodrucje { get; set; }

        public int EntityId
        {
            get { return DataId; }
            set { DataId = value; }
        }
    }
}