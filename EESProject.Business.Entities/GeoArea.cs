﻿using Core.Common.Contracts;
using Core.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business.Entities
{
    [DataContract]
    public class GeoArea : EntityBase, IIdentifiableEntity
    {
        [DataMember]
        public int LokId { get; set; }

        [DataMember]
        public string GeoSifra { get; set; }

        [DataMember]
        public string Naziv { get; set; }

        // Navigation properties
        public ICollection<Podatak> PodatakSet { get; set; }

        public int EntityId
        {
            get { return LokId; }
            set { LokId = value; }
        }
    }
}
