﻿using EESProject.Business.Common;
using EESProject.Business.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business.Tests
{
    [TestClass]
    public class FileReaderEngineTest
    {
        [TestMethod]
        public void TestReadFiles()
        {
            List<Podatak> podatki = new List<Podatak>()
            {
                new Podatak(){ DataId = 0, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(1990, 10, 5, 00, 14, 05) },
                new Podatak(){ DataId = 1, LocationCode = "BEO", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 2, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 3, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 6, 00, 00, 05) }
            };

            Mock<IFileReaderEngine> mockEngine = new Mock<IFileReaderEngine>();
            mockEngine.Setup(obj => obj.UpdateDatabase()).Returns("Database up to date");

            string message = mockEngine.Object.UpdateDatabase();
            Assert.IsTrue(message == "Database up to date");
        }
    }
}
