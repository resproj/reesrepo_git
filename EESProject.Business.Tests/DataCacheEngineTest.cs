﻿using Core.Common.Contracts;
using EESProject.Business.Business_Engines;
using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business.Tests
{
    [TestClass]
    public class DataCacheEngineTest
    {
        [TestMethod]
        public void test_data_caching()
        {
            List<Podatak> podatki = new List<Podatak>()
            {
                new Podatak(){ DataId = 0, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(1990, 10, 5, 00, 14, 05) },
                new Podatak(){ DataId = 1, LocationCode = "BEO", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 2, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 3, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 6, 00, 00, 05) }
            };
            string sifgeo = "SRB";
            DateTime pocetak = new DateTime(2016, 10, 5);
            DateTime kraj = new DateTime(2016, 10, 6);
            Mock<IPodatakRepository> mockPodatakRepository = new Mock<IPodatakRepository>();
            mockPodatakRepository.Setup(obj => obj.GetPodatakByQuery(sifgeo, pocetak, kraj)).Returns(
                                                        (from p in podatki
                                                         where (p.LocationCode.Equals(sifgeo) && p.Vreme > pocetak && p.Vreme < kraj)
                                                         select p).ToList());

            Mock<IDataRepositoryFactory> mockDataRepository = new Mock<IDataRepositoryFactory>();
            mockDataRepository.Setup(obj => obj.GetDataRepository<IPodatakRepository>()).Returns(mockPodatakRepository.Object);

            DataCacheEngine cache = new DataCacheEngine(mockDataRepository.Object);
                cache.GetDataByQuery(sifgeo, pocetak, kraj);                

            Assert.IsTrue(DataCacheEngine.cachedData != null);

        }
        [TestMethod]
        public void test_data_caching_functionality()
        {
            List<Podatak> podatki = new List<Podatak>()
            {
                new Podatak(){ DataId = 0, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(1990, 10, 5, 00, 14, 05) },
                new Podatak(){ DataId = 1, LocationCode = "BEO", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 2, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 3, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 6, 00, 00, 05) }
            };
            string sifgeo = "SRB";
            DateTime pocetak = new DateTime(2016, 10, 5);
            DateTime kraj = new DateTime(2016, 10, 6);
            Mock<IPodatakRepository> mockPodatakRepository = new Mock<IPodatakRepository>();
            mockPodatakRepository.Setup(obj => obj.GetPodatakByQuery(sifgeo, pocetak, kraj)).Returns(
                                                        (from p in podatki
                                                         where (p.LocationCode.Equals(sifgeo) && p.Vreme > pocetak && p.Vreme < kraj)
                                                         select p).ToList());

            Mock<IDataRepositoryFactory> mockDataRepository = new Mock<IDataRepositoryFactory>();
            mockDataRepository.Setup(obj => obj.GetDataRepository<IPodatakRepository>()).Returns(mockPodatakRepository.Object);

            //Assert.IsTrue(DataCacheEngine.cachedData == null);

            DateTime begin = DateTime.Now;
            {
                DataCacheEngine cache = new DataCacheEngine(mockDataRepository.Object);                
                cache.GetDataByQuery(sifgeo, pocetak, kraj);
                Assert.IsFalse(DataCacheEngine.cachedData == null);
            }

            Assert.IsTrue(DataCacheEngine.cachedData != null);

            DateTime end = DateTime.Now;
            TimeSpan firstTime = end - begin;

            begin = DateTime.Now;
            {
                DataCacheEngine cache = new DataCacheEngine(mockDataRepository.Object);
                cache.GetDataByQuery(sifgeo, pocetak, kraj);
            }
            end = DateTime.Now;
            TimeSpan secondTime = end - begin;

            Assert.IsTrue(secondTime <= firstTime);

        }
    }
}
