﻿using Core.Common.Contracts;
using EESProject.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Data.Contracts.RepositoryInterfaces
{
    public interface IPodatakRepository : IDataRepository<Podatak>
    {
        List<Podatak> GetPodatakByQuery(string sifgeo, DateTime pocetak, DateTime kraj);

        Podatak GetPodatakByLogicalKey(string sifgeo, DateTime vreme);
    }
}
