using Core.Common.Contracts;
using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using vstst = Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
//using NUnit.Framework;
using System.Collections.Generic;
using System;

namespace EESProject.Data.Tests
{
    // Unit tests with Visual Studio's UnitTesting which can be debugged :
    [vstst.TestClass]
    public class DataLayerTests
    {
        [vstst.TestInitialize]
        public void Initialize()
        {
        }

        [vstst.TestMethod]
        public void test_repository_mocking()
        {
            List<GeoArea> areas = new List<GeoArea>()
            {
                new GeoArea() {GeoSifra = "001", Naziv = "Beograd" },
                new GeoArea() {GeoSifra = "002", Naziv = "Kuzmin"}
            };

            Mock<IGeoAreaRepository> mockRepository = new Mock<IGeoAreaRepository>();
            mockRepository.Setup(obj => obj.Get()).Returns(areas);

            RepositoryTestClass repositoryTest = new RepositoryTestClass(mockRepository.Object);

            IEnumerable<GeoArea> ret = repositoryTest.GetAreas();

            vstst.Assert.IsTrue(ret == areas);
        }

        [vstst.TestMethod]
        public void test_factory_mocking()
        {
            List<GeoArea> areas = new List<GeoArea>()
            {
                new GeoArea() {GeoSifra = "001", Naziv = "Beograd" },
                new GeoArea() {GeoSifra = "002", Naziv = "Kuzmin"}
            };

            Mock<IDataRepositoryFactory> mockDataRepository = new Mock<IDataRepositoryFactory>();
            mockDataRepository.Setup(obj => obj.GetDataRepository<IGeoAreaRepository>().Get()).Returns(areas);

            RepositoryFactoryTestClass factoryTest = new RepositoryFactoryTestClass(mockDataRepository.Object);

            IEnumerable<GeoArea> ret = factoryTest.GetAreas();

            vstst.Assert.IsTrue(ret == areas);
        }


        [vstst.TestMethod]
        public void test_all_the_mocking()
        {
            List<GeoArea> areas = new List<GeoArea>()
            {
                new GeoArea() {GeoSifra = "001", Naziv = "Beograd" },
                new GeoArea() {GeoSifra = "002", Naziv = "Kuzmin"}
            };

            Mock<IGeoAreaRepository> mockGeoAreaRepository = new Mock<IGeoAreaRepository>();
            mockGeoAreaRepository.Setup(obj => obj.Get()).Returns(areas);

            Mock<IDataRepositoryFactory> mockDataRepository = new Mock<IDataRepositoryFactory>();
            mockDataRepository.Setup(obj => obj.GetDataRepository<IGeoAreaRepository>()).Returns(mockGeoAreaRepository.Object);

            RepositoryFactoryTestClass factoryTest = new RepositoryFactoryTestClass(mockDataRepository.Object);

            IEnumerable<GeoArea> ret = factoryTest.GetAreas();

            vstst.Assert.IsTrue(ret == areas);
        }
    }/**/

/*NUnit tests for CodeCoverage which don't let the project be built via pipeline:
    [TestFixture]
    public class DataLayerTestsNunit
    {
        [Test]
        public void test_repository_mocking()
        {
            List<GeoArea> areas = new List<GeoArea>()
            {
                new GeoArea() {GeoSifra = "001", Naziv = "Beograd" },
                new GeoArea() {GeoSifra = "002", Naziv = "Kuzmin"}
            };

            List<Podatak> podatki = new List<Podatak>()
            {
                new Podatak(){ DataId = 0, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(1990, 10, 5, 00, 14, 05) },
                new Podatak(){ DataId = 1, LocationCode = "BEO", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 2, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 3, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 6, 00, 00, 05) }
            };

            Mock<IGeoAreaRepository> mockGeoRepository = new Mock<IGeoAreaRepository>();
            mockGeoRepository.Setup(obj => obj.Get()).Returns(areas);

            Mock<IPodatakRepository> mockConsumptionRepository = new Mock<IPodatakRepository>();
            mockConsumptionRepository.Setup(obj => obj.Get()).Returns(podatki);

            RepositoryTestClass repositoryTest = new RepositoryTestClass(mockGeoRepository.Object);

            IEnumerable<GeoArea> ret = repositoryTest.GetAreas();

            repositoryTest = new RepositoryTestClass(mockConsumptionRepository.Object);
            IEnumerable<Podatak> consumption = repositoryTest.GetConsumption();

            Assert.IsTrue(ret == areas);
            Assert.IsTrue(consumption == podatki);
        }

        [Test]
        public void test_factory_mocking()
        {
            List<GeoArea> areas = new List<GeoArea>()
            {
                new GeoArea() {GeoSifra = "001", Naziv = "Beograd" },
                new GeoArea() {GeoSifra = "002", Naziv = "Kuzmin"}
            };
            List<Podatak> podatki = new List<Podatak>()
            {
                new Podatak(){ DataId = 0, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(1990, 10, 5, 00, 14, 05) },
                new Podatak(){ DataId = 1, LocationCode = "BEO", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 2, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 3, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 6, 00, 00, 05) }
            };

            Mock<IDataRepositoryFactory> mockDataRepository = new Mock<IDataRepositoryFactory>();
            mockDataRepository.Setup(obj => obj.GetDataRepository<IGeoAreaRepository>().Get()).Returns(areas);
            mockDataRepository.Setup(obj => obj.GetDataRepository<IPodatakRepository>().Get()).Returns(podatki);


            RepositoryFactoryTestClass factoryTest = new RepositoryFactoryTestClass(mockDataRepository.Object);

            IEnumerable<GeoArea> ret = factoryTest.GetAreas();
            IEnumerable<Podatak> dat = factoryTest.GetConsumption();

            Assert.IsTrue(ret == areas);
            Assert.IsTrue(dat == podatki);
        }


        [Test]
        public void test_all_the_mocking()
        {
            List<GeoArea> areas = new List<GeoArea>()
            {
                new GeoArea() {GeoSifra = "001", Naziv = "Beograd" },
                new GeoArea() {GeoSifra = "002", Naziv = "Kuzmin"}
            };

            List<Podatak> podatki = new List<Podatak>()
            {
                new Podatak(){ DataId = 0, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(1990, 10, 5, 00, 14, 05) },
                new Podatak(){ DataId = 1, LocationCode = "BEO", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 2, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 3, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 6, 00, 00, 05) }
            };

            Mock<IGeoAreaRepository> mockGeoRepository = new Mock<IGeoAreaRepository>();
            mockGeoRepository.Setup(obj => obj.Get()).Returns(areas);

            Mock<IPodatakRepository> mockConsumptionRepository = new Mock<IPodatakRepository>();
            mockConsumptionRepository.Setup(obj => obj.Get()).Returns(podatki);

            Mock<IDataRepositoryFactory> mockDataRepository = new Mock<IDataRepositoryFactory>();
            mockDataRepository.Setup(obj => obj.GetDataRepository<IGeoAreaRepository>()).Returns(mockGeoRepository.Object);
            mockDataRepository.Setup(obj => obj.GetDataRepository<IPodatakRepository>().Get()).Returns(podatki);

            RepositoryFactoryTestClass factoryTest = new RepositoryFactoryTestClass(mockDataRepository.Object);

            IEnumerable<GeoArea> ret = factoryTest.GetAreas();
            IEnumerable<Podatak> dat = factoryTest.GetConsumption();

            Assert.IsTrue(ret == areas);
            Assert.IsTrue(dat == podatki);
        }


    }*/
}
