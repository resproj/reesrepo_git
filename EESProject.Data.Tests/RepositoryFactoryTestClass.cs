﻿using Core.Common.Contracts;
using Core.Common.Core;
using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Data.Tests
{
    public class RepositoryFactoryTestClass
    {
        public RepositoryFactoryTestClass()
        {
            ObjectBase.Container.SatisfyImportsOnce(this);
        }

        public RepositoryFactoryTestClass(IDataRepositoryFactory dataRepositoryFactory)
        {
            _DataRepositoryFactory = dataRepositoryFactory;
        }

        [Import]
        IDataRepositoryFactory _DataRepositoryFactory;

        public IEnumerable<GeoArea> GetAreas()
        {
            IGeoAreaRepository _GeoRepository = _DataRepositoryFactory.GetDataRepository<IGeoAreaRepository>();

            IEnumerable<GeoArea> areas = _GeoRepository.Get();
            return areas;
        }
        public IEnumerable<Podatak> GetConsumption()
        {
            IPodatakRepository _PodatakRepository = _DataRepositoryFactory.GetDataRepository<IPodatakRepository>();

            IEnumerable<Podatak> data = _PodatakRepository.Get();
            return data;
        }
    }
}
