﻿using Core.Common.Core;
using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Data.Tests
{
    public class RepositoryTestClass
    {
        public RepositoryTestClass()
        {
            ObjectBase.Container.SatisfyImportsOnce(this);
        }

        public RepositoryTestClass(IGeoAreaRepository geoAreaRepository)
        {
            _GeoRepository = geoAreaRepository;
        }

        public RepositoryTestClass(IPodatakRepository consumptionRepository)
        {
            _ConsumptionRepository = consumptionRepository;
        }

        [Import]
        IGeoAreaRepository _GeoRepository;
        [Import]
        IPodatakRepository _ConsumptionRepository;

        public IEnumerable<GeoArea> GetAreas()
        {
            IEnumerable<GeoArea> ares = _GeoRepository.Get();

            return ares;
        }

        public IEnumerable<Podatak> GetConsumption()
        {
            IEnumerable<Podatak> data = _ConsumptionRepository.Get();

            return data;
        }
    }
}
