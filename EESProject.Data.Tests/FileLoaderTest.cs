﻿using Core.Common.Core;
using EESProject.Business.Bootstrapper;
using EESProject.Data.FileLoader;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Data.Tests
{
    [TestClass]
    public class FileLoaderTest
    {
        [TestInitialize]
        public void Initialize()
        {
            ObjectBase.Container = MEFLoader.Init();
        }
    }
}
