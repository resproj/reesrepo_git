﻿using Core.Common.Contracts;
using Core.Common.Core;
using EESProject.Business.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business.Providers
{
    public class DatabaseUpdateProvider : ProviderBase
    {
        public DatabaseUpdateProvider()
        {
            
        }
        public DatabaseUpdateProvider(IDataRepositoryFactory dataRepositoryFactory)
        {
            _dataRepositoryFactory = dataRepositoryFactory;
        }
        public DatabaseUpdateProvider(IBusinessEngineFactory businessEngineFactory)
        {
            _businessEngineFactory = businessEngineFactory;
        }
        public DatabaseUpdateProvider(IDataRepositoryFactory dataRepositoryFactory, IBusinessEngineFactory businessEngineFactory)
        {
            _dataRepositoryFactory = dataRepositoryFactory;
        }
        [Import]
        IDataRepositoryFactory _dataRepositoryFactory;
        [Import]
        IBusinessEngineFactory _businessEngineFactory;

        public string FileImport()
        {
            IFileReaderEngine fileReaderEngine = _businessEngineFactory.GetBusinessEngine<IFileReaderEngine>();
            string message = fileReaderEngine.UpdateDatabase();

            IGeoAreaCodeStorage geoAreaCodeStorage = _businessEngineFactory.GetBusinessEngine<IGeoAreaCodeStorage>();
            geoAreaCodeStorage.UpdateGeoAreaCodesList();

            return message;
        }


    }
}
