﻿using Core.Common.Contracts;
using Core.Common.Exceptions;
using EESProject.Business.Common;
using EESProject.Business.Contracts;
using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business.Providers
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall,
        ConcurrencyMode = ConcurrencyMode.Multiple/*,
        ReleaseServiceInstanceOnTransactionComplete = false*/)]
    public class EnergyConsumptionProvider : ProviderBase, IEnergyConsumptionService
    {
        //constructors
        public EnergyConsumptionProvider() { }
        public EnergyConsumptionProvider(IDataRepositoryFactory dataRepositoryFactory)
        {
            _dataRepositoryFactory = dataRepositoryFactory; 
        }
        public EnergyConsumptionProvider(IBusinessEngineFactory businessEngineFactory)
        {
            _businessEngineFactory = businessEngineFactory;
        }
        public EnergyConsumptionProvider(IDataRepositoryFactory dataRepositoryFactory, IBusinessEngineFactory businessEngineFactory)
        {
            _dataRepositoryFactory = dataRepositoryFactory;
            _businessEngineFactory = businessEngineFactory;
        }

        //factories
        [Import]
        IDataRepositoryFactory _dataRepositoryFactory;
        [Import]
        IBusinessEngineFactory _businessEngineFactory;

        #region Methods
        public Podatak[] DataRetrieval(string geoAreaCode, DateTime begin, DateTime end)
        {
            return ExecuteFaultHandledOperation(() =>
            {
                IDataCacheEngine dataCacheEngine = _businessEngineFactory.GetBusinessEngine<IDataCacheEngine>();
                List<Podatak> podatki = dataCacheEngine.GetDataByQuery(geoAreaCode, begin, end);

                return podatki.ToArray();
            });
        }

        public string OnDemandDatabaseUpdate()
        {
            Console.WriteLine("/nDatabase update called from client");
            string message;
            {
                DatabaseUpdateProvider updateProvider = new DatabaseUpdateProvider();
                message = updateProvider.FileImport();
            }
            Console.WriteLine(message);
            return message;
        }

        public string[] GetGeoAreaCodes()
        {
            IGeoAreaCodeStorage geoAreaCodeStorage = _businessEngineFactory.GetBusinessEngine<IGeoAreaCodeStorage>();
            return geoAreaCodeStorage.GetGeoAreaCodes().ToArray();
        }
        #endregion
    }
}
