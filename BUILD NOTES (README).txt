Build notes:

>Desktop and Host are the two start-up projects

>Desktop app should wait for Host to display the "DataBase up to date" message

>DataConnection string must be changed in three places in order to run the program and test it, 
	Host: App.config file
	Data: App.config file 
	Data.Tests: App.config file (for the integration test to work)

>The path to folder containing .csv files must be manually set in the EESProject.Business.Business Engines.FileReaderEngine in order for it to work 

>The UI is very ugly
>The tests are unfinished