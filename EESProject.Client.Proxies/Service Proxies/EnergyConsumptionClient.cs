﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using EESProject.Client.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using EESProject.Client.Entities;
using Core.Common.Exceptions;

namespace EESProject.Client.Proxies
{
    [Export(typeof(IEnergyConsumptionService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class EnergyConsumptionClient : ClientBase<IEnergyConsumptionService>, IEnergyConsumptionService
    {
        public Podatak[] DataRetrieval(string geoAreaCode, DateTime begin, DateTime end)
        {
            try
            {
                Podatak[] podaci = Channel.DataRetrieval(geoAreaCode, begin, end);
                return podaci;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string[] GetGeoAreaCodes()
        {
            string[] areas = Channel.GetGeoAreaCodes();
            return areas;
        }

        public string OnDemandDatabaseUpdate()
        {
            return Channel.OnDemandDatabaseUpdate();
        }
    }
}
