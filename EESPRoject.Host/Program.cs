﻿using Core.Common.Core;
using EESProject.Business.Bootstrapper;
using EESProject.Business.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace EESPRoject.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            ObjectBase.Container = MEFLoader.Init();

            Console.WriteLine("Updating Database");
            DatabaseUpdateProvider updateProvider = new DatabaseUpdateProvider();
            Console.WriteLine(updateProvider.FileImport());

            Console.WriteLine("Starting up service");
            Console.WriteLine();

            ServiceHost EnergyConsumptionHost = new ServiceHost(typeof(EnergyConsumptionProvider));

            StartService(EnergyConsumptionHost, "EnergyConsumptionProvider");

            Console.WriteLine("Press [ENTER] to close services");
            Console.ReadLine();
            StopService(EnergyConsumptionHost, "EnergyConsumptionProvider");
            Console.WriteLine("Press [ENTER] to exit");
            Console.ReadLine();
        }

        static void StartService(ServiceHost host, string serviceDescription)
        {
            host.Open();
            Console.WriteLine("Service '{0}' started.", serviceDescription);

            foreach (var endpoint in host.Description.Endpoints)
            {
                Console.WriteLine(string.Format("Listening on endpoint:"));
                Console.WriteLine(string.Format("Address: {0}", endpoint.Address.Uri.ToString()));
                Console.WriteLine(string.Format("Binding: {0}", endpoint.Binding.Name));
                Console.WriteLine(string.Format("Contract: {0}", endpoint.Contract.ConfigurationName));
            }

            Console.WriteLine();
        }

        static void StopService(ServiceHost host, string serviceDescription)
        {
            host.Close();
            Console.WriteLine("Service '{0}' stopped.", serviceDescription);
        }
    }
}
