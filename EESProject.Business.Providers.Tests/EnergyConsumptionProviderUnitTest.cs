﻿using Core.Common.Contracts;
using EESProject.Business.Business_Engines;
using EESProject.Business.Common;
using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
//using nu = NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business.Providers.Tests
{
    [TestClass]
    public class EnergyConsumptionProviderUnitTest
    {
        [TestInitialize]
        public void Initialize() { }

        [TestMethod]
        public void test_get_data_mock()
        {
            List<Podatak> podatki = new List<Podatak>()
            {
                new Podatak(){ DataId = 0, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(1990, 10, 5, 00, 14, 05) },
                new Podatak(){ DataId = 1, LocationCode = "BEO", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 2, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 3, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 6, 00, 00, 05) }
            };
            string sifgeo = "SRB";
            DateTime pocetak = new DateTime(2016, 10, 5);
            DateTime kraj = new DateTime(2016, 10, 6);
            Mock<IPodatakRepository> mockPodatakRepository = new Mock<IPodatakRepository>();
            mockPodatakRepository.Setup(obj => obj.GetPodatakByQuery(sifgeo, pocetak, kraj)).Returns(
                                                        (from p in podatki
                                                         where (p.LocationCode.Equals(sifgeo) && p.Vreme > pocetak && p.Vreme < kraj)
                                                         select p).ToList());

            Mock<IDataRepositoryFactory> mockDataRepositoryFactory = new Mock<IDataRepositoryFactory>();
            mockDataRepositoryFactory.Setup(obj => obj.GetDataRepository<IPodatakRepository>()).Returns(mockPodatakRepository.Object);

            Mock<IDataCacheEngine> mockCache = new Mock<IDataCacheEngine>();
            mockCache.Setup(obj => obj.GetDataByQuery(sifgeo, pocetak, kraj)).
                Returns(mockDataRepositoryFactory.Object.GetDataRepository<IPodatakRepository>().GetPodatakByQuery(sifgeo, pocetak, kraj));

            Mock<IBusinessEngineFactory> mockEngine = new Mock<IBusinessEngineFactory>();
            mockEngine.Setup(obj => obj.GetBusinessEngine<IDataCacheEngine>()).Returns(mockCache.Object);

            EnergyConsumptionProvider provider = new EnergyConsumptionProvider(mockDataRepositoryFactory.Object, mockEngine.Object);

            Podatak testPodatak = podatki[2];
            Podatak[] dobijeniPodaci = provider.DataRetrieval(sifgeo, pocetak, kraj);

            bool flag = true;
            foreach (Podatak item in dobijeniPodaci)
            {
                if (item.DataId != testPodatak.DataId)
                {
                    flag = false;
                }
            }

            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void test_get_data_mock_with_multiple_data()
        {
            List<Podatak> podatki = new List<Podatak>()
            {
                new Podatak(){ DataId = 0, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(1990, 10, 5, 00, 14, 05) },
                new Podatak(){ DataId = 1, LocationCode = "BEO", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 2, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 3, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 6, 00, 00, 05) },
                new Podatak(){ DataId = 4, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 06, 05) }
            };
            string sifgeo = "SRB";
            DateTime pocetak = new DateTime(2016, 10, 5);
            DateTime kraj = new DateTime(2016, 10, 6);
            Mock<IPodatakRepository> mockPodatakRepository = new Mock<IPodatakRepository>();
            mockPodatakRepository.Setup(obj => obj.GetPodatakByQuery(sifgeo, pocetak, kraj)).Returns(
                                                        (from p in podatki
                                                         where (p.LocationCode.Equals(sifgeo) && p.Vreme > pocetak && p.Vreme < kraj)
                                                         select p).ToList<Podatak>());

            Mock<IDataRepositoryFactory> mockDataRepositoryFactory = new Mock<IDataRepositoryFactory>();
            mockDataRepositoryFactory.Setup(obj => obj.GetDataRepository<IPodatakRepository>()).Returns(mockPodatakRepository.Object);

            Mock<IDataCacheEngine> mockCache = new Mock<IDataCacheEngine>();
            mockCache.Setup(obj => obj.GetDataByQuery(sifgeo, pocetak, kraj)).
                Returns(mockDataRepositoryFactory.Object.GetDataRepository<IPodatakRepository>().GetPodatakByQuery(sifgeo, pocetak, kraj));

            Mock<IBusinessEngineFactory> mockEngine = new Mock<IBusinessEngineFactory>();
            mockEngine.Setup(obj => obj.GetBusinessEngine<IDataCacheEngine>()).Returns(mockCache.Object);

            EnergyConsumptionProvider provider = new EnergyConsumptionProvider(mockDataRepositoryFactory.Object, mockEngine.Object);

            List<Podatak> testPodatak = (from p in podatki
                                         where (p.LocationCode.Equals(sifgeo) && p.Vreme > pocetak && p.Vreme < kraj)
                                         select p).ToList<Podatak>();

            List<Podatak> dobijeniPodaci = provider.DataRetrieval(sifgeo, pocetak, kraj).ToList();

            bool flag = true;
            for (int i = 0; i < dobijeniPodaci.Count; i++)
            {
                if (testPodatak[i].DataId != dobijeniPodaci[i].DataId)
                {
                    flag = false;
                }
            }

            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void test_get_data_mock_with_multiple_data_and_reentry()
        {
            List<Podatak> podatki = new List<Podatak>()
            {
                new Podatak(){ DataId = 0, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(1990, 10, 5, 00, 14, 05) },
                new Podatak(){ DataId = 1, LocationCode = "BEO", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 2, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 3, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 6, 00, 00, 05) },
                new Podatak(){ DataId = 4, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 06, 05) }
            };
            string sifgeo = "SRB";
            DateTime pocetak = new DateTime(2016, 10, 5);
            DateTime kraj = new DateTime(2016, 10, 6);
            Mock<IPodatakRepository> mockPodatakRepository = new Mock<IPodatakRepository>();
            mockPodatakRepository.Setup(obj => obj.GetPodatakByQuery(sifgeo, pocetak, kraj)).Returns(
                                                        (from p in podatki
                                                         where (p.LocationCode.Equals(sifgeo) && p.Vreme > pocetak && p.Vreme < kraj)
                                                         select p).ToList<Podatak>());

            Mock<IDataRepositoryFactory> mockDataRepositoryFactory = new Mock<IDataRepositoryFactory>();
            mockDataRepositoryFactory.Setup(obj => obj.GetDataRepository<IPodatakRepository>()).Returns(mockPodatakRepository.Object);

            Mock<IDataCacheEngine> mockCache = new Mock<IDataCacheEngine>();
            mockCache.Setup(obj => obj.GetDataByQuery(sifgeo, pocetak, kraj)).
                Returns(mockDataRepositoryFactory.Object.GetDataRepository<IPodatakRepository>().GetPodatakByQuery(sifgeo, pocetak, kraj));

            Mock<IBusinessEngineFactory> mockEngine = new Mock<IBusinessEngineFactory>();
            mockEngine.Setup(obj => obj.GetBusinessEngine<IDataCacheEngine>()).Returns(mockCache.Object);

            EnergyConsumptionProvider provider = new EnergyConsumptionProvider(mockDataRepositoryFactory.Object, mockEngine.Object);

            List<Podatak> testPodatak = (from p in podatki
                                         where (p.LocationCode.Equals(sifgeo) && p.Vreme > pocetak && p.Vreme < kraj)
                                         select p).ToList<Podatak>();


            DateTime begin = DateTime.Now;
            List<Podatak> dobijeniPodaci = provider.DataRetrieval(sifgeo, pocetak, kraj).ToList();
            DateTime end = DateTime.Now;
            TimeSpan firstTime = end - begin;

            begin = DateTime.Now;
            dobijeniPodaci = provider.DataRetrieval(sifgeo, pocetak, kraj).ToList();
            end = DateTime.Now;
            TimeSpan secondTime = end - begin;

            Assert.IsTrue(secondTime <= firstTime);

            bool flag = true;
            for (int i = 0; i < dobijeniPodaci.Count; i++)
            {
                if (testPodatak[i].DataId != dobijeniPodaci[i].DataId)
                {
                    flag = false;
                }
            }

            Assert.IsTrue(flag);
        }

        [TestMethod]
        public void test_database_update_ondemand_and_geo_area_storage()
        {
            Mock<IFileReaderEngine> mockReader = new Mock<IFileReaderEngine>();
            mockReader.Setup(obj => obj.UpdateDatabase()).Returns("Database up to date");

            List<GeoArea> geoAreas = new List<GeoArea>() {
                new GeoArea(){ Naziv = "SRB", GeoSifra = "SRB"},
                new GeoArea(){ Naziv = "BEG", GeoSifra = "BEG" } };

            Mock<IGeoAreaRepository> geoRepository = new Mock<IGeoAreaRepository>();
            geoRepository.Setup(obj => obj.Get()).Returns(geoAreas);

            Mock<IDataRepositoryFactory> dataRepositoryFactory = new Mock<IDataRepositoryFactory>();
            dataRepositoryFactory.Setup(obj => obj.GetDataRepository<IGeoAreaRepository>()).Returns(geoRepository.Object);

            GeoAreaCodeStorage mockStorage = new GeoAreaCodeStorage(dataRepositoryFactory.Object);

            Mock<IBusinessEngineFactory> mockEngine = new Mock<IBusinessEngineFactory>();
            mockEngine.Setup(obj => obj.GetBusinessEngine<IFileReaderEngine>()).Returns(mockReader.Object);
            mockEngine.Setup(obj => obj.GetBusinessEngine<IGeoAreaCodeStorage>()).Returns(mockStorage);

            DatabaseUpdateProvider updateProvider = new DatabaseUpdateProvider(mockEngine.Object);

            EnergyConsumptionProvider provider = new EnergyConsumptionProvider(mockEngine.Object);

            //provider.OnDemandDatabaseUpdate();
            updateProvider.FileImport();
            List<string> lista = provider.GetGeoAreaCodes().ToList();

            Assert.IsTrue(lista != null);

        }
    }

    /* nunit tests:
    [nu.TestFixture]
    public class NUnitTest
    {
        [nu.Test]
        public void test_database_update_ondemand()
        {
            Mock<IFileReaderEngine> mockReader = new Mock<IFileReaderEngine>();
            mockReader.Setup(obj => obj.UpdateDatabase()).Returns("Database up to date");

            List<GeoArea> geoAreas = new List<GeoArea>() {
                new GeoArea(){ Naziv = "SRB", GeoSifra = "SRB"},
                new GeoArea(){ Naziv = "BEG", GeoSifra = "BEG" } };

            Mock<IGeoAreaRepository> geoRepository = new Mock<IGeoAreaRepository>();
            geoRepository.Setup(obj => obj.Get()).Returns(geoAreas);

            Mock<IDataRepositoryFactory> dataRepositoryFactory = new Mock<IDataRepositoryFactory>();
            dataRepositoryFactory.Setup(obj => obj.GetDataRepository<IGeoAreaRepository>()).Returns(geoRepository.Object);

            GeoAreaCodeStorage mockStorage = new GeoAreaCodeStorage(dataRepositoryFactory.Object);

            Mock<IBusinessEngineFactory> mockEngine = new Mock<IBusinessEngineFactory>();
            mockEngine.Setup(obj => obj.GetBusinessEngine<IFileReaderEngine>()).Returns(mockReader.Object);
            mockEngine.Setup(obj => obj.GetBusinessEngine<IGeoAreaCodeStorage>()).Returns(mockStorage);

            DatabaseUpdateProvider updateProvider = new DatabaseUpdateProvider(mockEngine.Object);

            EnergyConsumptionProvider provider = new EnergyConsumptionProvider(mockEngine.Object);

            //provider.OnDemandDatabaseUpdate();
            updateProvider.FileImport();
            List<string> lista = provider.GetGeoAreaCodes().ToList();

            nu.Assert.IsTrue(lista != null);
        }

        [nu.Test]
        public void test_get_data_mock_with_multiple_data_and_reentry()
        {
            List<Podatak> podatki = new List<Podatak>()
            {
                new Podatak(){ DataId = 0, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(1990, 10, 5, 00, 14, 05) },
                new Podatak(){ DataId = 1, LocationCode = "BEO", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 2, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 00, 05) },
                new Podatak(){ DataId = 3, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 6, 00, 00, 05) },
                new Podatak(){ DataId = 4, LocationCode = "SRB", Vrednost = 5000, Vreme = new DateTime(2016, 10, 5, 00, 06, 05) }
            };
            string sifgeo = "SRB";
            DateTime pocetak = new DateTime(2016, 10, 5);
            DateTime kraj = new DateTime(2016, 10, 6);
            Mock<IPodatakRepository> mockPodatakRepository = new Mock<IPodatakRepository>();
            mockPodatakRepository.Setup(obj => obj.GetPodatakByQuery(sifgeo, pocetak, kraj)).Returns(
                                                        (from p in podatki
                                                         where (p.LocationCode.Equals(sifgeo) && p.Vreme > pocetak && p.Vreme < kraj)
                                                         select p).ToList<Podatak>());

            Mock<IDataRepositoryFactory> mockDataRepositoryFactory = new Mock<IDataRepositoryFactory>();
            mockDataRepositoryFactory.Setup(obj => obj.GetDataRepository<IPodatakRepository>()).Returns(mockPodatakRepository.Object);

            Mock<IDataCacheEngine> mockCache = new Mock<IDataCacheEngine>();
            mockCache.Setup(obj => obj.GetDataByQuery(sifgeo, pocetak, kraj)).
                Returns(mockDataRepositoryFactory.Object.GetDataRepository<IPodatakRepository>().GetPodatakByQuery(sifgeo, pocetak, kraj));

            Mock<IBusinessEngineFactory> mockEngine = new Mock<IBusinessEngineFactory>();
            mockEngine.Setup(obj => obj.GetBusinessEngine<IDataCacheEngine>()).Returns(mockCache.Object);

            EnergyConsumptionProvider provider = new EnergyConsumptionProvider(mockDataRepositoryFactory.Object, mockEngine.Object);

            List<Podatak> testPodatak = (from p in podatki
                                         where (p.LocationCode.Equals(sifgeo) && p.Vreme > pocetak && p.Vreme < kraj)
                                         select p).ToList<Podatak>();


            DateTime begin = DateTime.Now;
            List<Podatak> dobijeniPodaci = provider.DataRetrieval(sifgeo, pocetak, kraj).ToList();
            DateTime end = DateTime.Now;
            TimeSpan firstTime = end - begin;

            begin = DateTime.Now;
            dobijeniPodaci = provider.DataRetrieval(sifgeo, pocetak, kraj).ToList();
            end = DateTime.Now;
            TimeSpan secondTime = end - begin;

            nu.Assert.IsTrue(secondTime <= firstTime);

            bool flag = true;
            for (int i = 0; i < dobijeniPodaci.Count; i++)
            {
                if (testPodatak[i].DataId != dobijeniPodaci[i].DataId)
                {
                    flag = false;
                }
            }

            nu.Assert.IsTrue(flag);
        }
    }*/

}

