﻿using Core.Common.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business.Common
{
    public interface IGeoAreaCodeStorage : IBusinessEngine
    {
        void UpdateGeoAreaCodesList();

        List<string> GetGeoAreaCodes();
    }
}
