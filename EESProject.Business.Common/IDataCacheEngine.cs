﻿using Core.Common.Contracts;
using EESProject.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business.Common
{
    public interface IDataCacheEngine : IBusinessEngine
    {
        List<Podatak> GetDataByQuery(string geoId, DateTime begin, DateTime end);

        IDataRepository testMethod();
    }
}
