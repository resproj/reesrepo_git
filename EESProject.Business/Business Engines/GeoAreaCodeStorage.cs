﻿using Core.Common.Contracts;
using Core.Common.Exceptions;
using EESProject.Business.Common;
using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business.Business_Engines
{
    [Export(typeof(IGeoAreaCodeStorage))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class GeoAreaCodeStorage : IGeoAreaCodeStorage
    {
        public static List<string> GeoAreaCodes;
        private IDataRepositoryFactory dataRepositoryFactory;

        [ImportingConstructor]
        public GeoAreaCodeStorage(IDataRepositoryFactory dataRepositoryFactory)
        {
            if (GeoAreaCodes == null)
                GeoAreaCodes = new List<string>();
            this.dataRepositoryFactory = dataRepositoryFactory;
        }

        public List<string> GetGeoAreaCodes()
        {
            return GeoAreaCodes;
        }

        public void UpdateGeoAreaCodesList()
        {
            IGeoAreaRepository areaRepository = dataRepositoryFactory.GetDataRepository<IGeoAreaRepository>();

            IEnumerable<GeoArea> areas = areaRepository.Get();
            if (areas == null)
            {
                NotFoundException ex = new NotFoundException("No geo areas found in the database");
                throw new FaultException<NotFoundException>(ex, ex.Message);
            }
            foreach (GeoArea area in areas)
            {
                if (!GeoAreaCodes.Contains<string>(area.GeoSifra))
                {
                    GeoAreaCodes.Add(area.GeoSifra);
                }
            }
        }
    }
}
