﻿using Core.Common.Contracts;
using EESProject.Business.Common;
using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using EESProject.Data.FileLoader;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business
{
    [Export(typeof(IFileReaderEngine))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class FileReaderEngine : IFileReaderEngine
    {
        private FileReader reader;
        private List<Podatak> podaci;

        [ImportingConstructor]
        public FileReaderEngine(IDataRepositoryFactory dataRepositoryFactory)
        {
            _DataRepositoryFactory = dataRepositoryFactory;
        }

        IDataRepositoryFactory _DataRepositoryFactory;

        public string UpdateDatabase()
        {
            reader = new FileReader();
            podaci = new List<Podatak>();
            string message;
            try
            {
                string[] filenames = System.IO.Directory.GetFiles(@"C:\Users\Nippolis\Desktop\repo\csv");
                foreach (string file in filenames)
                {
                    podaci = reader.ParseFile(file);
                    WriteIntoDatabase(podaci);
                }
                message = "Database up to date";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return message;

        }

        private void WriteIntoDatabase(List<Podatak> podaci)
        {
            IGeoAreaRepository geoAreaRepository = _DataRepositoryFactory.GetDataRepository<IGeoAreaRepository>();
            IPodatakRepository podatakRepository = _DataRepositoryFactory.GetDataRepository<IPodatakRepository>();

            foreach (Podatak podatak in podaci)
            {
                GeoArea area = geoAreaRepository.GetEntityBySif(podatak.LocationCode);
                if (area == null)
                {
                    area = new GeoArea() { GeoSifra = podatak.LocationCode, Naziv = podatak.LocationCode };
                    area = geoAreaRepository.Add(area);
                }

                podatak.LokId = area.LokId;

                if (podatakRepository.GetPodatakByLogicalKey(podatak.LocationCode, podatak.Vreme)==null)
                {
                    podatakRepository.Add(podatak);
                }
            }
        }
    }
}
