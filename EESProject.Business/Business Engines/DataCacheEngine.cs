﻿using Core.Common.Contracts;
using Core.Common.Exceptions;
using EESProject.Business.Common;
using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Business.Business_Engines
{
    [Export(typeof(IDataCacheEngine))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DataCacheEngine : IDataCacheEngine/*, IDisposable*/
    {
        public static Dictionary<Tuple<string, Tuple<DateTime, DateTime>>, List<Podatak>> cachedData;

        [ImportingConstructor]
        public DataCacheEngine(IDataRepositoryFactory dataRepositoryFactory)
        {
            if (cachedData == null)
                cachedData = new Dictionary<Tuple<string, Tuple<DateTime, DateTime>>, List<Podatak>>();
            this.dataRepositoryFactory = dataRepositoryFactory;
        }

        public IDataRepository testMethod()
        {
            return dataRepositoryFactory.GetDataRepository<IPodatakRepository>();
        }

        private IDataRepositoryFactory dataRepositoryFactory;

        public List<Podatak> GetDataByQuery(string geoId, DateTime begin, DateTime end)
        {
            Tuple<DateTime, DateTime> timeRange = new Tuple<DateTime, DateTime>(begin, end);
            Tuple<string, Tuple<DateTime, DateTime>> cacheKey = new Tuple<string, Tuple<DateTime, DateTime>>(geoId, timeRange);

            if (cachedData.ContainsKey(cacheKey))
            {
                return cachedData[cacheKey];
            }
            else
            {
                IPodatakRepository podatakRepository = dataRepositoryFactory.GetDataRepository<IPodatakRepository>();

                List<Podatak> podatki = podatakRepository.GetPodatakByQuery(geoId, begin, end);
                if (podatki.Count == 0)
                {
                    NotFoundException ex = new NotFoundException("No data for given time period found");
                    throw new FaultException<NotFoundException>(ex, ex.Message);
                }
                cachedData.Add(cacheKey, podatki);

                return podatki;
            }
        }

        /*public void Dispose() { cachedData = null; }*/
    }
}
