﻿using Core.Common.UI.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Desktop.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MainViewModel : ViewModelBase
    {
        public static MainViewModel Master;
        public MainViewModel()
        {
            Master = this;
        }
        [Import]
        public DataObtainmentViewModel DataObtainmentViewModel { get; private set; }
        [Import]
        public DataGraphViewModel DataGraphViewModel { get; private set; }
    }
}
