﻿using Core.Common.Contracts;
using Core.Common.Extensions;
using Core.Common.UI.Core;
using EESProject.Client.Contracts;
using EESProject.Client.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EESProject.Desktop.ViewModels
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class DataObtainmentViewModel : ViewModelBase
    {
        #region BindingSourcePropertiesAndObjects
        public DelegateCommand<object> DisplayDataCommand { get; private set; }
        public DelegateCommand<object> UpdateDataCommand { get; private set; }
        public DelegateCommand<object> GetGeoAreasCommand { get; private set; }
        public DateTime Pocetak { get; set; }
        public DateTime Kraj { get; set; }

        private string _dBUpdateMessage;
        public string DBUpdateMessage { get => _dBUpdateMessage;
            set
            {
                if (_dBUpdateMessage != value)
                {
                    _dBUpdateMessage = value;
                    OnPropertyChanged(() => DBUpdateMessage, false);
                }
            }
        }

        private string _generalFeedbackMessage;
        public string GeneralFeedbackMessage
        {
            get => _generalFeedbackMessage;
            set
            {
                if (_generalFeedbackMessage != value)
                {
                    _generalFeedbackMessage = value;
                    OnPropertyChanged(() => GeneralFeedbackMessage, false);
                }
            }
        }

        public ObservableCollection<String> GeoAreaCodes { get; set; }

        private string _selectedCode;
        public String SelectedCode
        {
            get => _selectedCode;
            set
            {
                if (_selectedCode != value)
                {
                    _selectedCode = value;
                    OnPropertyChanged(() => SelectedCode, false);
                }
            }
        }
        ObservableCollection<Podatak> podaci;
        public ObservableCollection<Podatak> Podaci
        {
            get => podaci;
            set
            {
                if (podaci != value)
                {
                    podaci = value;
                    OnPropertyChanged(() => Podaci, false);
                }
            }
        }
        IServiceFactory _ServiceFactory;
        #endregion

        [ImportingConstructor]
        public DataObtainmentViewModel(IServiceFactory serviceFactory)
        {
            Pocetak = Kraj = DateTime.Now;
            _ServiceFactory = serviceFactory;
            Podaci = new ObservableCollection<Podatak>();
            DisplayDataCommand = new DelegateCommand<object>(OnDisplayDataCommandExecute, OnDisplayCommandCanExecute);
            UpdateDataCommand = new DelegateCommand<object>(OnUpdateDataCommandExecute);
            GetGeoAreasCommand = new DelegateCommand<object>(OnGetGeoAreasCommandExecute);
        }

        private void OnGetGeoAreasCommandExecute(object obj)
        {
            string[] codes = null;
            GeneralFeedbackMessage = "Waiting for GeoAreaCodes...";

                int timeoutNumber = 0;
                while (timeoutNumber < 3)
                {
                    try
                    {
                        WithClient<IEnergyConsumptionService>(_ServiceFactory.CreateClient<IEnergyConsumptionService>(), energyConsumptionClient =>
                        {
                            try
                            {
                                codes = energyConsumptionClient.GetGeoAreaCodes();
                            }
                            catch (Exception e)
                            {
                                GeneralFeedbackMessage = e.Message;
                            }
                        });
                        break;
                    }
                    catch (Exception e)
                    {
                        timeoutNumber++;
                    }
                }

            if (codes != null)
            {
                GeoAreaCodes.Merge(codes);
                GeneralFeedbackMessage = "Can begin work";
            }
            else
            {
                GeneralFeedbackMessage = "Failed to connect to Service";
            }
        }

        private void OnUpdateDataCommandExecute(object obj)
        {
            int timeout = 0;
            while (timeout < 5)
            {
                try
                {
                    WithClient<IEnergyConsumptionService>(_ServiceFactory.CreateClient<IEnergyConsumptionService>(), energyConsumptionClient =>
                    {
                        DBUpdateMessage = energyConsumptionClient.OnDemandDatabaseUpdate();
                    });
                    return;
                }
                catch (Exception)
                {
                    timeout++;
                }
            }
            DBUpdateMessage = "Unable to connect to service";
        }

        private bool OnDisplayCommandCanExecute(object obj)
        {
            if (Pocetak > Kraj || SelectedCode == null)
            {
                return false;
            }

            return true;
        }

        private void OnDisplayDataCommandExecute(object obj)
        {
            Podaci = new ObservableCollection<Podatak>();
            try
            {
                WithClient<IEnergyConsumptionService>(_ServiceFactory.CreateClient<IEnergyConsumptionService>(), energyConsumptionClient =>
                {
                    Podatak[] data = energyConsumptionClient.DataRetrieval(SelectedCode, Pocetak, Kraj);
                    if (data != null)
                    {
                        Podaci.Merge(data);
                    }
                });
            }
            catch (Exception ex)
            {
                GeneralFeedbackMessage = "No data found for given time period";
            }
            SelectedCode = null;
        }

        public override string ViewTitle
        {
            get { return "Obtainment"; }
        }

        protected override void OnViewLoaded()
        {
            GeoAreaCodes = new ObservableCollection<string>();

            GeneralFeedbackMessage = "Please import GeoAreas to begin work";
        }
    }
}
