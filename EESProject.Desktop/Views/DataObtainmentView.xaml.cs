﻿using Core.Common.UI.Core;
using EESProject.Desktop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EESProject.Desktop.Views
{
    /// <summary>
    /// Interaction logic for DataObtainmentView.xaml
    /// </summary>
    public partial class DataObtainmentView : UserControlViewBase
    {
        public DataObtainmentView()
        {
            InitializeComponent();
        }

        protected override void OnWireViewModelEvents(ViewModelBase viewModel)
        {
            DataObtainmentViewModel vm = viewModel as DataObtainmentViewModel;
            if (vm != null)
            {
               //vm.DoStuffAndPublish += Vm_DoStuffAndPublish;
            }
        }

        protected override void OnUnwireViewModelEvents(ViewModelBase viewModel)
        {
            DataObtainmentViewModel vm = viewModel as DataObtainmentViewModel;
            if (vm != null)
            {
                //vm.DoStuffAndPublish -= Vm_DoStuffAndPublish;
            }
        }

        private void Vm_DoStuffAndPublish(object sender, EventArgs e)
        {
            MessageBox.Show("Stuff done and published!");
        }
    }
}
