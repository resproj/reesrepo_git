﻿using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Data.DataRepositories
{
    [Export(typeof(IGeoAreaRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class GeoAreaRepository : DataRepositoryBase<GeoArea>, IGeoAreaRepository
    {
        public GeoArea GetEntityBySif(string sif)
        {
            using (EESProjectDBContext entityContext = new EESProjectDBContext())
            {
                return (from a in entityContext.GeoPodrucjeSet
                        where a.GeoSifra == sif
                        select a).FirstOrDefault();
            }
        }

        protected override GeoArea AddEntity(EESProjectDBContext entityContext, GeoArea entity)
        {
            return entityContext.GeoPodrucjeSet.Add(entity);
        }

        protected override IEnumerable<GeoArea> GetEntities(EESProjectDBContext entityContext)
        {
            return from e in entityContext.GeoPodrucjeSet
                   select e;
        }

        protected override GeoArea GetEntity(EESProjectDBContext entityContext, int id)
        {
            var query = (from e in entityContext.GeoPodrucjeSet
                         where e.LokId == id
                         select e);

            var results = query.FirstOrDefault();

            return results;
        }

        protected override GeoArea UpdateEntity(EESProjectDBContext entityContext, GeoArea entity)
        {
            return (from e in entityContext.GeoPodrucjeSet
                    where e.LokId == entity.LokId
                    select e).FirstOrDefault();
        }
    }
}
