﻿using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Data.DataRepositories
{
    [Export(typeof(IPodatakRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class PodatakRepository : DataRepositoryBase<Podatak>, IPodatakRepository
    {
        public Podatak GetPodatakByLogicalKey(string sifgeo, DateTime vreme)
        {
            using (EESProjectDBContext entityContext = new EESProjectDBContext())
            {
                return (from e in entityContext.PodatakSet
                        where e.LocationCode == sifgeo && e.Vreme == vreme
                        select e).FirstOrDefault();
            }
        }

        public List<Podatak> GetPodatakByQuery(string sifgeo, DateTime pocetak, DateTime kraj)
        {
            using (EESProjectDBContext entityContext = new EESProjectDBContext())
            {
                return (from e in entityContext.PodatakSet
                        where e.GeoPodrucje.GeoSifra == sifgeo && e.Vreme >= pocetak && e.Vreme <= kraj
                        select e).ToList<Podatak>();
            }
        }

        protected override Podatak AddEntity(EESProjectDBContext entityContext, Podatak entity)
        {
            return entityContext.PodatakSet.Add(entity);
        }

        protected override IEnumerable<Podatak> GetEntities(EESProjectDBContext entityContext)
        {
            return from e in entityContext.PodatakSet
                   select e;
        }

        protected override Podatak GetEntity(EESProjectDBContext entityContext, int id)
        {
            var query = (from e in entityContext.PodatakSet
                         where e.LokId == id
                         select e);

            var results = query.FirstOrDefault();

            return results;
        }

        protected override Podatak UpdateEntity(EESProjectDBContext entityContext, Podatak entity)
        {
            return (from e in entityContext.PodatakSet
                    where e.DataId == entity.DataId
                    select e).FirstOrDefault();
        }
    }
}
