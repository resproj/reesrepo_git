﻿using Core.Common.Contracts;
using EESProject.Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Data
{
    public class EESProjectDBContext : DbContext
    {
        public EESProjectDBContext() : base("EESProjDB")
        { 
            Database.SetInitializer<EESProjectDBContext>(null);
            //Database.SetInitializer<EESProjectDBContext>(new ProjekatDBInitializer<EESProjectDBContext>());
            //Database.Initialize(true);
            // https://stackoverflow.com/questions/33076291/onmodelcreating-is-never-called
        }

        public DbSet<GeoArea> GeoPodrucjeSet { get; set; }
        public DbSet<Podatak> PodatakSet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Ignore<PropertyChangedEventHandler>();
            modelBuilder.Ignore<ExtensionDataObject>();
            modelBuilder.Ignore<IIdentifiableEntity>();

            modelBuilder.Properties<string>().Configure(c => c.HasMaxLength(50));
            /*
            // GeoPodrucjeSet Table
            modelBuilder.Entity<GeoArea>().ToTable("GeoPodrucjeSet");
            modelBuilder.Entity<GeoArea>().HasKey<int>(e => e.LokId).Ignore(e => e.EntityId);
            modelBuilder.Entity<GeoArea>().Property(e => e.LokId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<GeoArea>().Property(e => e.GeoSifra).HasMaxLength(12);
            modelBuilder.Entity<GeoArea>().Property(e => e.Naziv).HasMaxLength(30);

            // PodatakSet Table
            modelBuilder.Entity<Podatak>().ToTable("PodatakSet");
            modelBuilder.Entity<Podatak>().HasKey<int>(e => e.DataId).Ignore(e => e.EntityId);
            modelBuilder.Entity<Podatak>().Property(e => e.DataId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            */

            // GeoPodrucjeSet Table
            modelBuilder.Entity<GeoArea>().ToTable("GeoPodrucjeSet");
            modelBuilder.Entity<GeoArea>().HasKey<int>(e => e.LokId).Ignore(e => e.EntityId);
            modelBuilder.Entity<GeoArea>().Property(e => e.LokId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<GeoArea>().Property(e => e.GeoSifra).HasMaxLength(12);
            modelBuilder.Entity<GeoArea>().Property(e => e.Naziv).HasMaxLength(30);

            // PodatakSet Table
            modelBuilder.Entity<Podatak>().ToTable("PodatakSet");
            modelBuilder.Entity<Podatak>().HasKey<int>(e => e.DataId).Ignore(e => e.EntityId);
            modelBuilder.Entity<Podatak>().Property(e => e.DataId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Podatak>().Property(e => e.LocationCode).HasMaxLength(12);

            // Configure Forein Key for one-to-many relationship
            modelBuilder.Entity<Podatak>()
                .HasRequired<GeoArea>(g => g.GeoPodrucje)
                .WithMany(t => t.PodatakSet)
                .HasForeignKey(f => f.LokId);
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        private class ProjekatDBInitializer<T> : DropCreateDatabaseAlways<EESProjectDBContext>
        {
            protected override void Seed(EESProjectDBContext context)
            {
                //#region GeoPodrucje

                //IList<GeoArea> geoPodrucjeStavke = new List<GeoArea>();

                //geoPodrucjeStavke.Add(new GeoArea()
                //{
                //    // LokId = 0, // DatabaseGeneratedOption.Identity
                //    GeoSifra = "0000",
                //    Naziv = "TEST"
                //});

                //foreach (var geoPodrucjeStavka in geoPodrucjeStavke)
                //{
                //    context.GeoPodrucjeSet.Add(geoPodrucjeStavka);
                //}

                //#endregion

                //#region Podatak

                //IList<Podatak> podatakStavke = new List<Podatak>();

                //podatakStavke.Add(new Podatak()
                //{
                //    // DataId = 0, // DatabaseGeneratedOption.Identity
                //    LokId = 1,
                //    LocationCode = "001",
                //    Vrednost = (decimal)0.15,
                //    Vreme = DateTime.Now
                //});

                //podatakStavke.Add(new Podatak()
                //{
                //    // DataId = 0, // DatabaseGeneratedOption.Identity
                //    LokId = 1,
                //    LocationCode = "001",
                //    Vrednost = (decimal)7.50,
                //    Vreme = DateTime.Now
                //});

                ////podatakStavke.Add(new Podatak()
                ////{
                ////	// DataId = 0, // DatabaseGeneratedOption.Identity
                ////	LokId = 32 /* LokId = 1, */
                ////	Vrednost = (decimal)20.30,
                ////	Vreme = DateTime.Now
                ////});
                //// System.Data.SqlClient.SqlException: The INSERT statement conflicted with 
                //// the FOREIGN KEY constraint "FK_dbo.PodatakSet_dbo.GeoPodrucjeSet_LokId".
                //// The conflict occurred in database "TPRojDB", table "dbo.GeoPodrucjeSet", column 'LokId'.
                //// The statement has been terminated.

                //foreach (var podatakStavka in podatakStavke)
                //{
                //    context.PodatakSet.Add(podatakStavka);
                //}

                //#endregion

                base.Seed(context);
            }
        }
    }
}
