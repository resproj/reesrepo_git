﻿using Core.Common.Contracts;
using Core.Common.Core;
using EESProject.Business.Entities;
using EESProject.Data.Contracts.RepositoryInterfaces;
using EESProject.Data.DataRepositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EESProject.Data.FileLoader
{
    public class FileReader
    {
        public List<Podatak> ParseFile(string file)
        {
            List<Podatak> podaci = new List<Podatak>();
            using (var textReader = new StreamReader(file))
            {
                int prelaz = 0;

                //dobijanje datuma
                string[] namecomponents = file.Split('\\');                
                string[] datecomponents = namecomponents[namecomponents.Length - 1].Split('_');
                string[] dan = datecomponents[3].Split('.');
                DateTime datum = new DateTime(int.Parse(datecomponents[1]), int.Parse(datecomponents[2]), int.Parse(dan[0]));
                if (datum.Month == 3 && datum.Day > 23  && datum.DayOfWeek == DayOfWeek.Sunday)
                    prelaz = 1;
                if (datum.Month == 10 && datum.Day > 23 && datum.DayOfWeek == DayOfWeek.Sunday)
                    prelaz = 2;
                
                //preskakanje zaglavlja
                string line = textReader.ReadLine();
                int skipCount = 0;
                while (line != null && skipCount < 1)
                {
                    line = textReader.ReadLine();
                    skipCount++;
                }
                
                while (line != null)
                {
                    string[] columns = line.Split('\t');
                    podaci.Add(TryParseData(columns, datum, prelaz));
                    line = textReader.ReadLine();
                }
                if (!((podaci.Count == 23 && prelaz == 1) || (podaci.Count == 24 && prelaz == 0) || (podaci.Count == 25 && prelaz == 2)))
                {
                    throw new Exception("Invalid number of hours in a day");
                }

                return podaci;
            }
        }

        public Podatak TryParseData(string[] columns, DateTime datum, int prelazni)
        {
            Podatak podatak = new Podatak();
            int year = datum.Year;
            int month = datum.Month;
            int day = datum.Day;
            int hour = int.Parse(columns[0]);
            int minute = 0;

            try
            {
                switch (prelazni)
                {
                    case (0):
                        if (hour == 24)
                        {
                            hour = 0;
                            day++;
                        }
                        break;
                    case (1):
                        if (hour == 1)
                            hour = 2;
                        else
                            hour++;
                        break;
                    case (2):
                        if (hour > 2)
                        {
                            hour--;
                        }
                        else if (hour == 2)
                        {
                            hour = 1;
                            minute = 1;
                        }
                        break;
                    default:
                        break;
                }
                podatak.Vreme = new DateTime(year, month, day, hour, minute, 0);
                podatak.Vrednost = (decimal.Parse(columns[1]));
                podatak.LocationCode = columns[2];
            }
            catch (Exception)
            {
                throw new Exception("Error parsing file");               
            }

            return podatak;
        }
    }
    
}
