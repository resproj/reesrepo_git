﻿using Core.Common.Core;

namespace EESProject.Client.Entities
{
    public class GeoArea : ObjectBase
    {
        private int lokId;
        private string geoSifra;
        private string naziv;

        public int LokId
        {
            get => lokId;
            set
            {
                if (lokId != value)
                {
                    lokId = value;
                    //OnPropertyChanged(() => LokId);
                    OnPropertyChanged(nameof(LokId));
                }
            }
        }
        public string GeoSifra
        {
            get => geoSifra;
            set
            {
                if (geoSifra != value)
                {
                    geoSifra = value;
                    OnPropertyChanged(() => GeoSifra);
                }
            }
        }
        public string Naziv
        {
            get => naziv;
            set
            {
                if (naziv != value)
                {
                    naziv = value;
                    OnPropertyChanged(() => Naziv);
                }
            }
        }
    }
}
