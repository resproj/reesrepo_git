﻿using Core.Common.Core;
using System;

namespace EESProject.Client.Entities
{
    public class Podatak : ObjectBase
    {
        private int _dataId;
        private int _lokId;
        private string _locationCode;
        private decimal _vrednost;
        private DateTime _vreme;

        public int DataId
        {
            get => _dataId;
            set
            {
                if (_dataId != value)
                {
                    _dataId = value;
                    OnPropertyChanged(() => DataId);
                }
            }
        }

        public int LokId
        {
            get => _lokId;
            set
            {
                if (_lokId != value)
                {
                    _lokId = value;
                    OnPropertyChanged(() => LokId);
                }
            }
        }

        public string LocationCode
        {
            get => _locationCode;
            set
            {
                if (_locationCode != value)
                {
                    _locationCode = value;
                    OnPropertyChanged(() => LocationCode);
                }
            }
        }

        public decimal Vrednost
        {
            get => _vrednost;
            set
            {
                if (_vrednost != value)
                {
                    _vrednost = value;
                    OnPropertyChanged(() => Vrednost);
                }
            }
        }

        public DateTime Vreme
        {
            get => _vreme;
            set
            {
                if (_vreme != value)
                {
                    _vreme = new DateTime(value.Year, value.Month, value.Day, value.Hour, value.Minute, value.Second);
                    OnPropertyChanged(() => Vreme);
                }
            }
        }

    }
}